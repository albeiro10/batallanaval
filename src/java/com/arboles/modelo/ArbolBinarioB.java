/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.modelo;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.validadores.InfanteValidador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carloaiza
 */
public class ArbolBinarioB {
    private NodoABB raiz;
    private int cantidadNodos;

    public ArbolBinarioB() {
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }
    
    public void adicionarNodo(Infante dato) throws InfanteExcepcion
    {
        InfanteValidador.verificarDatosObligatorios(dato);
        InfanteValidador.verificarEdadInfante(dato.getEdad());
        NodoABB nodo= new NodoABB(dato);
        if(raiz==null)
        {
            raiz=nodo;
            cantidadNodos++;
        }
        else
        {
            adicionarNodo(nodo,raiz);
            cantidadNodos++;
        }
    }
    
    private void adicionarNodo(NodoABB nuevo, NodoABB pivote) throws InfanteExcepcion
    {
        //Seleccionar el camino
        if(nuevo.getDato().getNroIdentificacion()
                .compareTo(pivote.getDato().getNroIdentificacion())==0)
        {                
          throw new InfanteExcepcion("Ya existe un infante con la "
                  + "identificación "+nuevo.getDato().getNroIdentificacion());
        }
        else if(nuevo.getDato().getNroIdentificacion()
                .compareTo(pivote.getDato().getNroIdentificacion())<0)
        {
            ///Va para la izq
            if(pivote.getIzquierda()==null)
            {
                pivote.setIzquierda(nuevo);
            }
            else
            {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }    
        }
        else
        {
            //va para la derecha
            if(pivote.getDerecha()==null)
            {
                pivote.setDerecha(nuevo);
            }
            else
            {
                adicionarNodo(nuevo, pivote.getDerecha());
            } 
        }    
        
    }
    
     public List<Infante> recorrerInOrden() throws InfanteExcepcion {
        if(esVacio())
        {
            throw  new InfanteExcepcion("El árbol está vació");
        }    
        List<Infante> listaInfantes=new ArrayList<>();
        recorrerInOrden(raiz,listaInfantes);
        return listaInfantes;
    }

    private void recorrerInOrden(NodoABB reco,List<Infante> listaInfantes) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(),listaInfantes);
            listaInfantes.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(),listaInfantes);
        }
    }
    
    public List<Infante> recorrerPreOrden() throws InfanteExcepcion {
        if(esVacio())
        {
            throw  new InfanteExcepcion("El árbol está vació");
        }    
        List<Infante> listaInfantes=new ArrayList<>();
        recorrerPreOrden(raiz,listaInfantes);
        return listaInfantes;
    }

    private void recorrerPreOrden(NodoABB reco,List<Infante> listaInfantes) {
        if (reco != null) {
            listaInfantes.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(),listaInfantes);            
            recorrerPreOrden(reco.getDerecha(),listaInfantes);
        }
    }
    
    public List<Infante> recorrerPostOrden() throws InfanteExcepcion {
        if(esVacio())
        {
            throw  new InfanteExcepcion("El árbol está vació");
        }    
        List<Infante> listaInfantes=new ArrayList<>();
        recorrerPostOrden(raiz,listaInfantes);
        return listaInfantes;
    }

    private void recorrerPostOrden(NodoABB reco,List<Infante> listaInfantes) {
        if (reco != null) {
            
            recorrerPostOrden(reco.getIzquierda(),listaInfantes);            
            recorrerPostOrden(reco.getDerecha(),listaInfantes);
            listaInfantes.add(reco.getDato());
        }
    }
    
    
    
    public boolean esVacio()
    {
        return raiz==null;
    }
    
    
}
