/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.controladores;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.modelo.ArbolBinarioB;
import com.arboles.modelo.Infante;
import com.arboles.modelo.Localidad;
import com.arboles.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author carloaiza
 */
@Named(value = "controladorABB")
@SessionScoped
public class ControladorABB implements Serializable {

    private ArbolBinarioB arbol = new ArbolBinarioB();
    private Infante infante= new Infante();
    private List<Localidad> localidades;
    
    private boolean verRegistrar;
    private DefaultDiagramModel model;
    
    /**
     * Creates a new instance of ControladorABB
     */
    public ControladorABB() {
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }
    
    
    
    @PostConstruct
    public void inicializar()
    {
        localidades = new ArrayList<>();
        localidades.add(new Localidad("16917", "Caldas"));
        localidades.add(new Localidad("16017001", "Manizales"));
        localidades.add(new Localidad("05", "Antioquia"));
        localidades.add(new Localidad("16005001", "Medellín"));
        
        verRegistrar=false;
        pintarArbol();
    }

    public ArbolBinarioB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolBinarioB arbol) {
        this.arbol = arbol;
    }

    public Infante getInfante() {
        return infante;
    }

    public void setInfante(Infante infante) {
        this.infante = infante;
    }
    
    public void habilitarVerRegistrar()
    {
        verRegistrar= true;
    }
    
    public void deshabilitarVerRegistrar()
    {
        verRegistrar = false;
    }
    
    
    public void pintarArbol()
    {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
                 
        Element ceo = new Element("CEO", "25em", "6em");
        ceo.addEndPoint(createEndPoint(EndPointAnchor.BOTTOM));
        model.addElement(ceo);
         
        //CFO
        Element cfo = new Element("CFO", "10em", "18em");
        cfo.addEndPoint(createEndPoint(EndPointAnchor.TOP));
        cfo.addEndPoint(createEndPoint(EndPointAnchor.BOTTOM));
         
        Element fin = new Element("FIN", "5em", "30em");
        fin.addEndPoint(createEndPoint(EndPointAnchor.TOP));
         
        Element pur = new Element("PUR", "20em", "30em");
        pur.addEndPoint(createEndPoint(EndPointAnchor.TOP));
         
        model.addElement(cfo);
        model.addElement(fin);
        model.addElement(pur);
         
        //CTO
        Element cto = new Element("CTO", "40em", "18em");
        cto.addEndPoint(createEndPoint(EndPointAnchor.TOP));
        cto.addEndPoint(createEndPoint(EndPointAnchor.BOTTOM));
         
        Element dev = new Element("DEV", "35em", "30em");
        dev.addEndPoint(createEndPoint(EndPointAnchor.TOP));
         
        Element tst = new Element("TST", "50em", "30em");
        tst.addEndPoint(createEndPoint(EndPointAnchor.TOP));
         
        model.addElement(cto);
        model.addElement(dev);
        model.addElement(tst);
         
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:3}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
                         
        //connections
        model.connect(new Connection(ceo.getEndPoints().get(0), cfo.getEndPoints().get(0), connector));        
        model.connect(new Connection(ceo.getEndPoints().get(0), cto.getEndPoints().get(0), connector));
        model.connect(new Connection(cfo.getEndPoints().get(1), fin.getEndPoints().get(0), connector));
        model.connect(new Connection(cfo.getEndPoints().get(1), pur.getEndPoints().get(0), connector));
        model.connect(new Connection(cto.getEndPoints().get(1), dev.getEndPoints().get(0), connector));
        model.connect(new Connection(cto.getEndPoints().get(1), tst.getEndPoints().get(0), connector));
    }
    
     private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");
         
        return endPoint;
    }
     
    public DiagramModel getModel() {
        return model;
    }
    
    public void guardarInfante()
    {
        try {
            arbol.adicionarNodo(infante);
            infante= new Infante();
            verRegistrar= false;
            pintarArbol();
        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
    
}
