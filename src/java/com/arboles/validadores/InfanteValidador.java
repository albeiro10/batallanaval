/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.validadores;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.modelo.Infante;
import com.arboles.modelo.Localidad;
import java.util.List;

/**
 *
 * @author carloaiza
 */
public class InfanteValidador {
    public static void verificarDatosObligatorios(Infante infante) 
            throws InfanteExcepcion
    {
        if(infante.getNroIdentificacion()==null || 
                infante.getNroIdentificacion().equals("")
                || infante.getNroIdentificacion().startsWith(" "))
        {
            throw new InfanteExcepcion("Debe diligenciar el número de "
                    + "identificación");
        }
        if(infante.getEdad()==0)
        {
            throw new InfanteExcepcion("Debe diligenciar la edad");
        }    
        if(infante.getNombre()==null || 
                infante.getNombre().equals("")
                || infante.getNombre().startsWith(" "))
        {
            throw new InfanteExcepcion("Debe diligenciar el nombre");
        }
        
    }
    
    public static void verificarEdadInfante(byte edad) throws InfanteExcepcion
    {
        if(edad <=0 || edad >7)
        {
            throw  new InfanteExcepcion("La edad debe estar entre 1 y 7");
        }    
    }
    
    public static void verificarLocalidad(Localidad localidad, 
            List<Localidad> localidades) throws InfanteExcepcion
    {
        boolean band=false;
        for(Localidad loc: localidades)
        {
            if(loc.getCodigo().equals(localidad.getCodigo()))
            {
                band=true;
                break;
            }
        }
        if(!band)
        {
           throw  new InfanteExcepcion("La localidad ingresada no existe");
        }
    }
}
